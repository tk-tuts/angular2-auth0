/**
 * Created by Keerthikan on 21-Feb-17.
 */
import { Component } from '@angular/core';
import {Auth} from "../../auth.service";

@Component({
  moduleId: module.id,
  selector: 'profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent {
  profile: any;

  constructor(private auth: Auth){
    this.profile = JSON.parse(localStorage.getItem('profile'));
    console.log(this.profile);
  }
}
